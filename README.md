PostCard extra for MODX

## Quickstart

1. Clone this project under MODX directory, for example, `Extras/PostCard`
2. Install node dependencies in `./_frontend` with `yarn` or `npm`
3. Install php dependencies in `./core/components/postcard` with `composer`
4. Make development symlinks from repository to MODX installation:
- `./assets/components/postcard` -> `../../assets/components/postcard`
- `./core/components/postcard` -> `../../core/components/postcard`
5. Run `php ./_build/build.php` for build and install package.

Now you can run `yarn dev` in `./_frontend` and see changes in PostCard section of MODX manager.
