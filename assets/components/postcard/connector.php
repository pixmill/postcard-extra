<?php

if (!defined('MODX_CORE_PATH')) {
    $path = __DIR__;
    while (!file_exists($path . '/config.core.php') && (strlen($path) > 1)) {
        $path = dirname($path);
    }
    require $path . '/config.core.php';
}

/** @var modX $modx */
require MODX_CORE_PATH . 'components/postcard/bootstrap.php';

$container = new DI\Container();
$container->set('modX', $modx); // Add modX to container dependencies
$app = DI\Bridge\Slim\Bridge::create($container);
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
require MODX_CORE_PATH . 'components/postcard/routes.php';

try {
    // Change request variables to make FastRoute work as usual
    $key = $modx->getOption('request_param_alias', null, 'q');
    $_SERVER['REQUEST_URI'] = $_REQUEST[$key];
    $_SERVER['QUERY_STRING'] = html_entity_decode($_SERVER['QUERY_STRING']);
    $app->run();
} catch (Throwable $e) {
    http_response_code($e->getCode());
    echo json_encode($e->getMessage());
}