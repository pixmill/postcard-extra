<?php

$_lang['area_postcard_main'] = 'Main';

$_lang['setting_postcard_phone_mask'] = 'Phone Mask';
$_lang['setting_postcard_phone_mask_desc'] = 'Mask for phone input fields in forms. You can use underscore as placeholder for numbers.';

$_lang['setting_postcard_max_text_length'] = 'Text Length';
$_lang['setting_postcard_max_text_length_desc'] = 'The maximum length of PostCard message.';

$_lang['setting_postcard_allow_emoji'] = 'Allow Emoji';
$_lang['setting_postcard_allow_emoji_desc'] = 'By default all emoji are excluded from message.';

$_lang['setting_postcard_pdf_tpl'] = 'PDF chunk';
$_lang['setting_postcard_pdf_tpl_desc'] = 'Specify a chunk to render a pdf with a postcard. You can use Fenom and MODX syntax.';

$_lang['setting_postcard_load_mgr_css'] = 'Load manager CSS';
$_lang['setting_postcard_load_mgr_css_desc'] = 'Load Bootstrap styles for PostCard manager section. If you disable this setting, you will need load your own styles.';

$_lang['setting_postcard_email_notify'] = 'Email for notifications';
$_lang['setting_postcard_email_notify_desc'] = 'If you specify an email here, then new records will be sent on it';