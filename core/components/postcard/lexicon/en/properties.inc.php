<?php

$_lang['postcard_prop_tpl'] = 'Chunk with postcard form to load.';
$_lang['postcard_prop_loadJs'] = 'Load VueJs scripts. If disabled, you will need to submit form manually.';
$_lang['postcard_prop_loadCss'] = 'Load Bootstrap styles. If disabled you will need to style form yourself.';
$_lang['postcard_prop_extended'] = 'You are using extended version of form with inline Vue code.';
