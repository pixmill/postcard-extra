<?php

include_once 'setting.inc.php';

$_lang['postcard.title'] = 'PostCard';
$_lang['postcard.menu_desc'] = 'A postcards management section';
$_lang['postcard.intro_msg'] = 'Here you can manage users postcards';
$_lang['postcard.submit_success'] = 'Your postcard was submitted successfully!';
$_lang['postcard.email_subject'] = 'Your have new postcard!';

$_lang['models.postcard.id'] = 'Id';
$_lang['models.postcard.title'] = 'Title';
$_lang['models.postcard.text'] = 'Text';
$_lang['models.postcard.name_for'] = 'Receiver Name';
$_lang['models.postcard.name_from'] = 'Sender Name';
$_lang['models.postcard.email_from'] = 'Sender Email';
$_lang['models.postcard.phone_from'] = 'Sender Phone';
$_lang['models.postcard.processed'] = 'Processed';
$_lang['models.postcard.file'] = 'Image';
$_lang['models.postcard.file_upload'] = 'Upload image...';
$_lang['models.postcard.file_replace'] = 'Replace uploaded image...';
$_lang['models.postcard.delete'] = 'Delete postcard';
$_lang['models.postcard.delete_confirm'] = 'Are you sure you want to delete this postcard?';

$_lang['actions.preview'] = 'Preview';
$_lang['actions.preview_close'] = 'Close Preview';
$_lang['actions.create'] = 'Create';
$_lang['actions.submit'] = 'Submit';
$_lang['actions.cancel'] = 'Cancel';
$_lang['actions.edit'] = 'Edit';
$_lang['actions.delete'] = 'Delete';
$_lang['actions.send_another'] = 'Send a new postcard';

$_lang['components.table.query'] = 'Search...';
$_lang['components.table.no_data'] = 'Nothing to display';
$_lang['components.table.no_results'] = 'Nothing found';
$_lang['components.table.records'] = 'No records | 1 record | {total} records';
$_lang['components.confirm_yes'] = 'Yes';
$_lang['components.confirm_no'] = 'No';
$_lang['components.confirm_delete_title'] = 'Confirmation required';
$_lang['components.confirm_delete_message'] = 'Are you sure you want to delete this entry?';