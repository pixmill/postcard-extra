<?php

$_lang['area_postcard_main'] = 'Hauptsächlich';

$_lang['setting_postcard_phone_mask'] = 'Telefonmaske';
$_lang['setting_postcard_phone_mask_desc'] = 'Maske für Telefoneingabefelder in Formularen. Sie können den Unterstrich als Platzhalter für Zahlen verwenden.';

$_lang['setting_postcard_max_text_length'] = 'Textlänge';
$_lang['setting_postcard_max_text_length_desc'] = 'Die maximale Länge einer PostCard-Nachricht.';

$_lang['setting_postcard_allow_emoji'] = 'Emoticons zulassen';
$_lang['setting_postcard_allow_emoji_desc'] = 'Standardmäßig sind alle Emojis von der Nachricht ausgeschlossen.';

$_lang['setting_postcard_pdf_tpl'] = 'PDF-Stück';
$_lang['setting_postcard_pdf_tpl_desc'] = 'Geben Sie einen Chunk an, um ein PDF mit einer Postkarte zu rendern. Sie können Fenom- und MODX-Syntax verwenden.';

$_lang['setting_postcard_load_mgr_css'] = 'Lademanager-CSS';
$_lang['setting_postcard_load_mgr_css_desc'] = 'Laden Sie Bootstrap-Stile für den PostCard-Manager-Bereich. Wenn Sie diese Einstellung deaktivieren, müssen Sie Ihre eigenen Stile laden.';

$_lang['setting_postcard_email_notify'] = 'E-Mail für Benachrichtigungen';
$_lang['setting_postcard_email_notify_desc'] = 'Wenn Sie hier eine E-Mail angeben, werden neue Datensätze darauf gesendet';