<?php

$_lang['postcard_prop_tpl'] = 'Chunk mit Postkartenformular zum Laden.';
$_lang['postcard_prop_loadJs'] = 'Laden Sie VueJs-Skripte. Wenn deaktiviert, müssen Sie das Formular manuell einreichen.';
$_lang['postcard_prop_loadCss'] = 'Laden Sie Bootstrap-Stile. Wenn deaktiviert, müssen Sie das Formular selbst gestalten.';
$_lang['postcard_prop_extended'] = 'Sie verwenden die erweiterte Version des Formulars mit Inline-Vue-Code.';
