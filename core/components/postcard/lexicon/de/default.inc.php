<?php

include_once 'setting.inc.php';

$_lang['postcard.title'] = 'Postkarte';
$_lang['postcard.menu_desc'] = 'Ein Abschnitt zur Verwaltung von Postkarten';
$_lang['postcard.intro_msg'] = 'Hier können Sie Benutzerpostkarten verwalten';
$_lang['postcard.submit_success'] = 'Ihre Postkarte wurde erfolgreich übermittelt!';
$_lang['postcard.email_subject'] = 'Sie haben eine neue Postkarte!';

$_lang['models.postcard.id'] = 'Id';
$_lang['models.postcard.title'] = 'Titel';
$_lang['models.postcard.text'] = 'Text';
$_lang['models.postcard.name_for'] = 'Empfängername';
$_lang['models.postcard.name_from'] = 'Absender';
$_lang['models.postcard.email_from'] = 'Absender E-Mail';
$_lang['models.postcard.phone_from'] = 'Absender Telefon';
$_lang['models.postcard.processed'] = 'Verarbeitet';
$_lang['models.postcard.file'] = 'Bild';
$_lang['models.postcard.file_upload'] = 'Bild hochladen...';
$_lang['models.postcard.file_replace'] = 'Hochgeladenes Bild ersetzen...';
$_lang['models.postcard.delete'] = 'Postkarte löschen';
$_lang['models.postcard.delete_confirm'] = 'Möchten Sie diese Postkarte wirklich löschen?';

$_lang['actions.preview'] = 'Vorschau';
$_lang['actions.preview_close'] = 'Vorschau schließen';
$_lang['actions.create'] = 'Erstellen';
$_lang['actions.submit'] = 'Einreichen';
$_lang['actions.cancel'] = 'Abbrechen';
$_lang['actions.edit'] = 'Bearbeiten';
$_lang['actions.delete'] = 'Löschen';
$_lang['actions.send_another'] = 'Eine weitere Postkarte schicken';

$_lang['components.table.query'] = 'Suche...';
$_lang['components.table.no_data'] = 'Nichts zu zeigen';
$_lang['components.table.no_results'] = 'Nichts gefunden';
$_lang['components.table.records'] = 'Ich erinnere mich nicht | 1 Rekord | {total} Rekorde';
$_lang['components.confirm_yes'] = 'Ja';
$_lang['components.confirm_no'] = 'Nein';
$_lang['components.confirm_delete_title'] = 'Bestätigung erforderlich';
$_lang['components.confirm_delete_message'] = 'Möchten Sie diesen Eintrag wirklich löschen?';