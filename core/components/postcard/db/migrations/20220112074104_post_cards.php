<?php

use Illuminate\Database\Schema\Blueprint;
use Vesp\Services\Migration;

class PostCards extends Migration
{
    public function up(): void
    {
        $this->schema->create(
            'files',
            function (Blueprint $table) {
                $table->id();
                $table->string('file');
                $table->string('path');
                $table->string('title')->nullable();
                $table->string('type')->nullable();
                $table->smallInteger('width')->unsigned()->nullable();
                $table->smallInteger('height')->unsigned()->nullable();
                $table->integer('size')->unsigned()->nullable();
                $table->text('metadata')->nullable();
                $table->timestamps();
            }
        );

        $this->schema->create(
            'post_cards',
            function (Blueprint $table) {
                $table->id();
                $table->string('title');
                $table->text('text');
                $table->string('name_for');
                $table->string('name_from');
                $table->string('email_from', 100)->nullable();
                $table->string('phone_from', 20)->nullable();
                $table->foreignId('file_id')->nullable()
                    ->constrained('files')->onDelete('set null');
                $table->boolean('processed')->default(false)->index();
                $table->uuid('uuid')->nullable()->unique();
                $table->timestamps();
                $table->timestamp('processed_at')->nullable();

                $table->index(['uuid', 'created_at']);
            }
        );
    }

    public function down(): void
    {
        $this->schema->drop('post_cards');
        $this->schema->drop('files');
    }
}
