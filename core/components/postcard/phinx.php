<?php

include_once 'bootstrap.php';

use Vesp\Services\Eloquent;
use Vesp\Services\Migration;

return [
    'paths' => [
        'migrations' => __DIR__ . '/db/migrations',
        'seeds' => __DIR__ . '/db/seeds',
    ],
    'migration_base_class' => Migration::class,
    'environments' => [
        'default_migration_table' => getenv('DB_PREFIX') . 'migrations',
        'default_environment' => 'dev',
        'dev' => [
            'name' => getenv('DB_DATABASE'),
            'connection' => (new Eloquent())->getConnection()->getPdo(),
        ],
    ],
];