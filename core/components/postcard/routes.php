<?php

use Slim\Routing\RouteCollectorProxy;

/** @var Slim\App $app */
$app->group(
    '/admin',
    function (RouteCollectorProxy $group) {
        $group->any('/post-cards[/{id:\d+}]', PostCard\Controllers\Admin\PostCards::class);
        $group->any('/post-cards/{id:\d+}/preview', PostCard\Controllers\Admin\PostCardPreview::class);
    }
);

$app->group(
    '/web',
    function (RouteCollectorProxy $group) {
        $group->any('/post-cards/{uuid}', PostCard\Controllers\Web\PostCards::class);
        $group->any('/post-cards/{uuid}/preview', PostCard\Controllers\Web\PostCardPreview::class);
    }
);

$app->get('/image/{id:\d+}', Vesp\Controllers\Data\Image::class);