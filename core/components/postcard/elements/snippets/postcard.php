<?php

/** @var modX $modx */

/** @var array $scriptProperties */
$loadJs = $modx->getOption('loadJs', $scriptProperties, true);
$loadCss = $modx->getOption('loadCss', $scriptProperties, true);
$extended = $modx->getOption('extended', $scriptProperties, false);
$tpl = $modx->getOption('tpl', $scriptProperties, '');

require_once MODX_CORE_PATH . 'components/postcard/bootstrap.php';
$PostCard = new PostCard\PostCard($modx, $scriptProperties);

$config = [
    'connectorUrl' => $PostCard->config['connectorUrl'],
    'phoneMask' => $PostCard->config['phoneMask'],
    'maxTextLength' => $PostCard->config['maxTextLength'],
    'allowEmoji' => (bool)$PostCard->config['allowEmoji'],
];
$modx->regClientStartupScript('<script>PostCard={Config: ' . json_encode($config) . '}</script>', true);

$manifest = file_get_contents($PostCard->config['assetsPath'] . 'manifest.json');
if ($files = json_decode($manifest, true)) {
    if ($loadCss) {
        $modx->regClientCSS($PostCard->config['assetsUrl'] . $files['web.css']);
    }
    if ($loadJs) {
        $modx->regClientScript(
            $PostCard->config['assetsUrl'] . ($extended ? $files['web-extended.js'] : $files['web.js'])
        );
    }
}

return (class_exists('pdoTools') ? $modx->getService('pdoTools') : $modx)->getChunk($tpl, $config);