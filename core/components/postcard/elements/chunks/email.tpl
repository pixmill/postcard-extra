<p><strong>Titel:</strong> {$title}</p>

<p><strong>Text:</strong> {$text}</p>

<p><strong>Empfängername:</strong> {$name_for}</p>

<p><strong>Absender:</strong> {$name_from}</p>

{if $email_from}
<p><strong>Absender E-Mail:</strong> {$email_from}</p>
{/if}

{if $phone_from}
<p><strong>Absender Telefon:</strong> {$phone_from}</p>
{/if}
