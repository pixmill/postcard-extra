<?php

namespace PostCard;

use modX;

class PostCard
{
    public modX $modx;
    public array $config = [];

    public function __construct(modX $modx, array $config = [])
    {
        $this->modx = $modx;
        $corePath = MODX_CORE_PATH . 'components/postcard/';
        $assetsUrl = MODX_ASSETS_URL . 'components/postcard/';
        $assetsPath = MODX_ASSETS_PATH . 'components/postcard/';
        $connectorUrl = $assetsUrl . 'connector.php?' . $this->modx->getOption('request_param_alias', null, 'q') . '=';

        $this->config = array_merge([
            'corePath' => $corePath,
            'assetsPath' => $assetsPath,
            'assetsUrl' => $assetsUrl,
            'connectorUrl' => $connectorUrl,
            'phoneMask' => $modx->getOption('postcard_phone_mask'),
            'maxTextLength' => $modx->getOption('postcard_max_text_length'),
            'allowEmoji' => (bool)$modx->getOption('postcard_allow_emoji'),
        ], $config);

        $this->modx->lexicon->load('postcard:default');
    }
}