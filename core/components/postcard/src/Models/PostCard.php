<?php

namespace PostCard\Models;

use Carbon\Carbon;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use modX;

/**
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $name_for
 * @property string $name_from
 * @property ?string $email_from
 * @property ?string $phone_from
 * @property ?int $file_id
 * @property bool $processed
 * @property ?string $uuid
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $processed_at
 *
 * @property-read File $file
 */
class PostCard extends Model
{
    protected $guarded = ['id', 'uuid', 'updated_at', 'created_at'];
    protected $casts = ['processed' => 'bool'];
    protected $dates = ['updated_at', 'created_at', 'processed_at'];

    protected static function booted(): void
    {
        static::deleting(
            static function (self $record) {
                if ($record->file) {
                    $record->file->delete();
                }
            }
        );
    }

    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class);
    }

    public function getPdf(modX $modx): string
    {
        $data = $this->toArray();
        $data['lang'] = $modx->getOption('cultureKey');
        if ($this->file) {
            $data['file'] = $this->file->toArray();
            if ($image = $this->file->getFile()) {
                $data['image'] = 'data:' . $this->file->type . ';base64,' . base64_encode($image);
            }
        }

        /** @var modX $service */
        $service = class_exists('pdoTools') ? $modx->getService('pdoTools') : $modx;
        $tpl = $modx->getOption('postcard_pdf_tpl', null, 'tpl.PostCard.pdf');
        $html = $service->getChunk($tpl, $data);

        $options = new Options([
            'tempDir' => MODX_CORE_PATH . 'cache/postcard/',
            'fontCache' => MODX_CORE_PATH . 'cache/postcard/',
            'isRemoteEnabled' => true,
        ]);
        if (!file_exists($options->getTempDir())) {
            mkdir($options->getTempDir());
        }
        $dompdf = new Dompdf($options);
        $dompdf->setPaper('A5');
        $dompdf->loadHtml($html);
        $dompdf->render();

        return $dompdf->output();
    }
}