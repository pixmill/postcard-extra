<?php

namespace PostCard\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;

class File extends \Vesp\Models\File
{
    protected static function booted(): void
    {
        static::saving(
            static function (self $record) {
                if ($record->metadata && is_array($record->metadata)) {
                    $record->metadata = json_encode($record->metadata);
                }
            }
        );
    }

    public function postCards(): HasMany
    {
        return $this->hasMany(PostCard::class);
    }
}
