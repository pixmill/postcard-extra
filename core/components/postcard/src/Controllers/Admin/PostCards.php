<?php

namespace PostCard\Controllers\Admin;

use Illuminate\Database\Eloquent\Builder;
use PostCard\Controllers\Traits\FileObjectControllerTrait;
use PostCard\Controllers\Traits\ManagerControllerTrait;
use PostCard\Models\PostCard;
use Vesp\Controllers\ModelController;

class PostCards extends ModelController
{
    use ManagerControllerTrait;
    use FileObjectControllerTrait;

    protected $model = PostCard::class;
    protected array $attachments = ['file'];

    protected function beforeGet(Builder $c): Builder
    {
        $c->whereNull('uuid');
        $c->with('file:id,updated_at');

        return $c;
    }

    protected function beforeCount(Builder $c): Builder
    {
        $c->whereNull('uuid');

        if ($query = $this->getProperty('query')) {
            $c->where(static function (Builder $c) use ($query) {
                $c->where('title', 'LIKE', "%$query%");
                $c->orWhere('text', 'LIKE', "%$query%");
                $c->orWhere('name_for', 'LIKE', "%$query%");
                $c->orWhere('name_from', 'LIKE', "%$query%");
                $c->orWhere('email_from', 'LIKE', "%$query%");
            });
        }

        return $c;
    }

    protected function afterCount(Builder $c): Builder
    {
        return $c->with('file:id,updated_at');
    }
}