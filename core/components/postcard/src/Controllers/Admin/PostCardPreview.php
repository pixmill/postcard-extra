<?php

namespace PostCard\Controllers\Admin;

use GuzzleHttp\Psr7\Utils;
use PostCard\Controllers\Traits\ManagerControllerTrait;
use PostCard\Models\PostCard;
use Psr\Http\Message\ResponseInterface;
use Vesp\Controllers\ModelGetController;

class PostCardPreview extends ModelGetController
{
    use ManagerControllerTrait;

    protected $model = PostCard::class;

    public function get(): ResponseInterface
    {
        /** @var PostCard $card */
        if (!$card = PostCard::query()->whereNull('uuid')->find($this->getProperty('id'))) {
            return $this->failure('Not Found', 404);
        }

        $pdf = $card->getPdf($this->modx);
        $stream = Utils::streamFor($pdf);

        return $this->response
            ->withBody($stream)
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Disposition', 'inline;filename=postcard-' . $card->id . '.pdf');
    }
}