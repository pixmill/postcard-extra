<?php

namespace PostCard\Controllers\Web;

use Illuminate\Database\Eloquent\Builder;
use PostCard\Controllers\Traits\FileObjectControllerTrait;
use PostCard\Models\PostCard;
use Psr\Http\Message\ResponseInterface;
use Vesp\Controllers\ModelController;
use Vesp\Services\Eloquent;
use modMail;
use modX;

class PostCards extends ModelController
{
    use FileObjectControllerTrait;

    protected $model = PostCard::class;
    protected array $attachments = ['file'];

    protected modX $modx;

    public function __construct(Eloquent $eloquent, modX $modx)
    {
        parent::__construct($eloquent);
        $this->modx = $modx;
    }

    protected function beforeGet(Builder $c): Builder
    {
        $c->where('uuid', $this->getProperty('uuid'));
        $c->with('file:id,updated_at');

        return $c;
    }

    protected function beforeCount(Builder $c): Builder
    {
        $c->where('uuid', $this->getProperty('uuid'));

        return $c;
    }

    public function post(): ?ResponseInterface
    {
        $uuid = $this->getProperty('uuid');
        $preview = $this->getProperty('preview');
        if (!$card = PostCard::query()->where('uuid', $uuid)->first()) {
            $card = new PostCard();
        }
        $card->uuid = $preview ? $uuid : null;
        $card->fill($this->getProperties());
        $this->processFiles($card);
        $card->save();
        if (!$preview) {
            $this->sendEmail($card);
        }

        return $this->success();
    }

    protected function sendEmail(PostCard $card)
    {
        if (!$to = $this->modx->getOption('postcard_email_notify')) {
            return;
        }

        /** @var pdoTools $pdo */
        $pdo = $this->modx->getService('pdoTools');
        $body = $pdo->getChunk('tpl.PostCard.email', $card->toArray());

        $this->modx->lexicon->load('postcard:default');
        /** @var \modPHPMailer $mail */
        $mail = $this->modx->getService('mail', 'mail.modPHPMailer');

        $mail->setHTML(true);
        $mail->set(modMail::MAIL_SUBJECT, $this->modx->lexicon('postcard.email_subject'));
        $mail->set(modMail::MAIL_BODY, $body);
        $mail->set(modMail::MAIL_SENDER, $this->modx->getOption('emailsender'));
        $mail->set(modMail::MAIL_FROM, $this->modx->getOption('emailsender'));
        $mail->set(modMail::MAIL_FROM_NAME, $this->modx->getOption('site_name'));
        $mail->address('to', $to);

        if ($tmp = tempnam(sys_get_temp_dir(), 'pdf_')) {
            file_put_contents($tmp, $card->getPdf($this->modx));
            $mail->attach($tmp, 'postcard-' . $card->id . '.pdf');
        }

        if (!$mail->send()) {
            $this->modx->log(
                \xPDO::LOG_LEVEL_ERROR,
                'An error occurred while trying to send the email: ' . $mail->mailer->ErrorInfo
            );
        }
        $mail->reset();
        @unlink($tmp);
    }
}