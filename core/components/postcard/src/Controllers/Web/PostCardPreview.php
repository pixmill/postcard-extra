<?php

namespace PostCard\Controllers\Web;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Utils;
use modX;
use PostCard\Models\PostCard;
use Psr\Http\Message\ResponseInterface;
use Vesp\Controllers\ModelGetController;
use Vesp\Services\Eloquent;

class PostCardPreview extends ModelGetController
{
    protected $model = PostCard::class;
    protected modX $modx;

    public function __construct(Eloquent $eloquent, modX $modx)
    {
        parent::__construct($eloquent);
        $this->modx = $modx;
    }

    public function get(): ResponseInterface
    {
        $this::clearDrafts();

        /** @var PostCard $card */
        if (!$card = PostCard::query()->where('uuid', $this->getProperty('uuid'))->first()) {
            return $this->failure('Not Found', 404);
        }

        $pdf = $card->getPdf($this->modx);
        $stream = Utils::streamFor($pdf);

        return $this->response
            ->withBody($stream)
            ->withHeader('Content-Type', 'application/pdf')
            ->withHeader('Content-Disposition', 'inline;filename=postcard-' . $card->uuid . '.pdf');
    }

    public static function clearDrafts(int $days = 1): void
    {
        $c = PostCard::query()->whereNotNull('uuid')->where('created_at', '<', Carbon::now()->subDays($days));
        /** @var PostCard $card */
        foreach ($c->get() as $card) {
            if ($card->file) {
                $card->file->delete();
            }
            $card->delete();
        }
    }
}