<?php

namespace PostCard\Controllers\Traits;

use modX;
use Psr\Http\Message\ResponseInterface;
use Vesp\Services\Eloquent;

/**
 * @method failure(string $message, int $code = 422)
 */
trait ManagerControllerTrait
{
    protected modX $modx;

    public function __construct(Eloquent $eloquent, modX $modx)
    {
        parent::__construct($eloquent);
        $modx->initialize('mgr');

        $ml = $modx->getOption('manager_language', null, 'en');
        $modx->lexicon->load($ml . ':core:default');
        $modx->setOption('cultureKey', $ml);

        $this->modx = $modx;
    }

    public function checkScope(string $method): ?ResponseInterface
    {
        if (!$this->modx->user || !$this->modx->user->hasSessionContext('mgr')) {
            return $this->failure($this->modx->lexicon('access_denied'), 401);
        }

        return null;
    }
}