<?php

namespace PostCard\Controllers\Traits;

use Exception;
use Illuminate\Database\Eloquent\Model;
use PostCard\Models\File;
use Psr\Http\Message\ResponseInterface;

/**
 * @method getProperty(string $key, $default = null)
 * @method failure(string $message, int $code = 422)
 * @property array $attachments
 */
trait FileObjectControllerTrait
{
    protected function processFiles(Model $record): ?ResponseInterface
    {
        foreach ($this->attachments as $attachment) {
            $file = $record->$attachment;
            // Delete existed attachment if received false
            if ($file && $this->getProperty("new_$attachment") === false) {
                $file->delete();
            } elseif ($tmp = $this->getProperty("new_$attachment", $this->getProperty($attachment))) {
                /** @var File $file */
                if (!$file) {
                    $file = new File();
                }

                try {
                    // Process base64 encoded file
                    if (!empty($tmp['file']) && $file->uploadFile($tmp['file'], $tmp['metadata'])) {
                        $record->{"{$attachment}_id"} = $file->id;

                        // If file is already uploaded, move it
                        if (!empty($tmp['temporary'])) {
                            $filesystem = $file->getFilesystem()->getBaseFilesystem();
                            $path = rtrim(sys_get_temp_dir(), '/') . '/' . $tmp['id'];
                            if (file_exists($path) && $stream = fopen($path, 'rb')) {
                                if ($filesystem->updateStream($file->getFilePathAttribute(), $stream)) {
                                    unlink($path);
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    return $this->failure($e->getMessage());
                }
            }
        }

        return null;
    }

    protected function beforeSave(Model $record): ?ResponseInterface
    {
        if ($error = $this->processFiles($record)) {
            return $error;
        }

        return null;
    }

}
