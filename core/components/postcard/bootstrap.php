<?php

/** @var modX $modx */
if (!defined('MODX_CORE_PATH')) {
    $path = __DIR__;
    while (!file_exists($path . '/config.core.php') && (strlen($path) > 1)) {
        $path = dirname($path);
    }
    require_once $path . '/config.core.php';
}

if (!class_exists('modX') || !isset($modx)) {
    require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
    define('MODX_API_MODE', true);
    require_once MODX_BASE_PATH . 'index.php';
}

putenv('DB_DRIVER=' . $modx->config['dbtype']);
putenv('DB_HOST=' . $modx->config['host']);
putenv('DB_PORT=' . (@$modx->config['port'] ?: '3306'));
putenv('DB_PREFIX=' . $modx->config['table_prefix'] . 'pc_');
putenv('DB_DATABASE=' . $modx->config['dbname']);
putenv('DB_USERNAME=' . $modx->config['username']);
putenv('DB_PASSWORD=' . $modx->config['password']);
putenv('DB_CHARSET=' . $modx->config['charset']);
putenv('DB_COLLATION=' . $modx->config['charset'] . '_general_ci');
putenv('DB_FOREIGN_KEYS=1');
putenv('UPLOAD_DIR=' . MODX_ASSETS_PATH . 'components/postcard/upload/');
putenv('CACHE_DIR=' . MODX_CORE_PATH . 'cache/postcard/');

require_once __DIR__ . '/vendor/autoload.php';