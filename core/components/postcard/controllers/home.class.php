<?php

use PostCard\PostCard;

class PostCardHomeManagerController extends modExtraManagerController
{
    public PostCard $PostCard;

    public function initialize(): void
    {
        $modx = $this->modx;
        include_once MODX_CORE_PATH . 'components/postcard/bootstrap.php';
        $this->PostCard = new PostCard($modx);
        parent::initialize();
    }

    public function getLanguageTopics(): array
    {
        return ['postcard:default'];
    }

    public function getPageTitle(): ?string
    {
        return $this->modx->lexicon('postcard.title');
    }

    public function loadCustomCssJs(): void
    {
        $this->addHtml('<script>PostCardConfig = ' . json_encode($this->PostCard->config) . '</script>');

        $manifest = file_get_contents($this->PostCard->config['assetsPath'] . 'manifest.json');
        if ($files = json_decode($manifest, true)) {
            $this->addJavascript($this->PostCard->config['assetsUrl'] . $files['mgr.js']);
            if ($this->modx->getOption('postcard_load_mgr_css', null, true)) {
                $this->addCss($this->PostCard->config['assetsUrl'] . $files['mgr.css']);
            }
        }
    }

    public function getTemplateFile(): string
    {
        $this->content .= '<div id="postcard-app-root"></div>';

        return '';
    }
}