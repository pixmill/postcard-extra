import Vue from './web-common'

const App = new Vue(require('./web/script.js').default)

document.addEventListener('DOMContentLoaded', () => {
  App.$mount(document.getElementById('postcard-app'))
})
