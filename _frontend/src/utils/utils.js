export const translate = (key, placeholders = {}) => {
  let result = window._(key)
  if (!result) {
    console.warn('Could not load language key: ' + key)
    return key
  }
  Object.keys(placeholders).forEach((key) => {
    result = result.replace(`{${key}}`, placeholders[key])
  })
  return result
}

export const translateForms = (key, choice, placeholders = {}) => {
  let strings = window._(key)
  if (!strings) {
    console.warn('Could not load language key: ' + key)
    return key
  }
  strings = strings.split('|').map((i) => i.trim())
  const choicesLength = strings.length
  let variant = 0
  choice = Math.abs(choice)

  if (choicesLength > 3) {
    if (choice === 0) {
      return 0
    }
    const teen = choice > 10 && choice < 20
    const endsWithOne = choice % 10 === 1
    if (choicesLength < 4) {
      return !teen && endsWithOne ? 1 : 2
    }
    if (!teen && endsWithOne) {
      return 1
    }
    if (!teen && choice % 10 >= 2 && choice % 10 <= 4) {
      return 2
    }
    variant = choicesLength < 4 ? 2 : 3
  } else if (choicesLength === 2) {
    variant = choice ? (choice > 1 ? 1 : 0) : 1
  } else {
    variant = choice ? Math.min(choice, 2) : 0
  }

  let result = strings[variant]
  Object.keys(placeholders).forEach((key) => {
    result = result.replace(`{${key}}`, placeholders[key])
  })

  return result
}
