import Vue from 'vue'
import axios from 'axios'
import {v4 as uuid} from 'uuid'
import './web/styles.scss'

import {BFormFile, BFormInput, BSpinner} from 'bootstrap-vue'
import VespInputTextMask from '@vesp/frontend/components/inputs/text-mask'

Vue.component('BFormInput', BFormInput)
Vue.component('BFormFile', BFormFile)
Vue.component('BSpinner', BSpinner)
Vue.component('VespInputTextMask', VespInputTextMask)

Vue.prototype.$axios = axios
Vue.prototype.$uuid = uuid

export default Vue
