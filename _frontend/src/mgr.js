import Vue from 'vue'
import axios from 'axios'
import VueRouter from 'vue-router'
import './mgr/styles.scss'

import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {faCheck, faEdit, faEye, faPlus, faSync, faTimes} from '@fortawesome/free-solid-svg-icons'

import {
  BForm,
  BFormCheckbox,
  BFormFile,
  BFormGroup,
  BFormInput,
  BFormTextarea,
  BImg,
  BLink,
  BOverlay,
  BPagination,
  BSpinner,
  BTable,
  ButtonPlugin,
  InputGroupPlugin,
  LayoutPlugin,
  ModalPlugin,
} from 'bootstrap-vue'

import VespTable from '@vesp/frontend/components/table'
import VespModal from '@vesp/frontend/components/modal'
import VespInputTextMask from '@vesp/frontend/components/inputs/text-mask'
import {imageLink} from '@vesp/frontend/plugins/utils'
import {translate, translateForms} from './utils/utils'

library.add(faSync, faTimes, faEdit, faPlus, faCheck, faEye)
Vue.component('Fa', FontAwesomeIcon)

Vue.use(VueRouter)
Vue.use(LayoutPlugin)
Vue.use(ModalPlugin, {BModal: {static: true}})
Vue.use(ButtonPlugin)
Vue.use(InputGroupPlugin)
Vue.component('BForm', BForm)
Vue.component('BFormGroup', BFormGroup)
Vue.component('BFormInput', BFormInput)
Vue.component('BFormTextarea', BFormTextarea)
Vue.component('BOverlay', BOverlay)
Vue.component('BSpinner', BSpinner)
Vue.component('BFormCheckbox', BFormCheckbox)
Vue.component('BFormFile', BFormFile)
Vue.component('BTable', BTable)
Vue.component('BPagination', BPagination)
Vue.component('BLink', BLink)
Vue.component('BImg', BImg)

Vue.component('VespTable', VespTable)
Vue.component('VespModal', VespModal)
Vue.component('VespInputTextMask', VespInputTextMask)

Vue.prototype.$axios = axios
Vue.prototype.$t = translate
Vue.prototype.$tc = translateForms
Vue.prototype.$image = (file, options) => {
  const link = imageLink(file, options, Vue.prototype.$axios.defaults.baseURL + '/image')
  let t = 0
  return link.replace(/\?/g, (match) => (++t >= 2 ? '&' : match))
}

const router = new VueRouter({
  base: location.pathname + location.search,
  routes: [
    {
      path: '/',
      name: 'cards',
      component: require('./mgr/pages/cards.vue').default,
      children: [
        {
          path: '/create',
          name: 'cards-create',
          component: require('./mgr/pages/cards/create.vue').default,
        },
        {
          path: '/edit/:id',
          name: 'cards-edit-id',
          component: require('./mgr/pages/cards/edit/_id.vue').default,
        },
        {
          path: '/delete/:id',
          name: 'cards-delete-id',
          component: require('./mgr/pages/cards/delete/_id.vue').default,
        },
      ],
    },
  ],
})

// eslint-disable-next-line no-undef
Ext.onReady(() => {
  // eslint-disable-next-line no-undef
  axios.defaults.baseURL = window.PostCardConfig.connectorUrl

  new Vue({
    el: '#postcard-app-root',
    router,
    render: (h) => h(require('./mgr/templates/default.vue').default),
  })
})
