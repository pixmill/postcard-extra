export default {
  data() {
    return {
      file: null,
      record: {},
      phoneMask: window.PostCard.Config.phoneMask || '+41 __ ___ __ __',
      allowEmoji: Boolean(window.PostCard.Config.allowEmoji || false),
      maxLength: Number(window.PostCard.Config.maxTextLength || 400),
      previewLoading: false,
      submitLoading: false,
      showPreview: false,
      showSuccess: false,
    }
  },
  created() {
    window.PostCard.Config.sessionKey = this.$uuid()
    this.$axios.defaults.baseURL = window.PostCard.Config.connectorUrl
  },
  computed: {
    url() {
      return '/web/post-cards/' + window.PostCard.Config.sessionKey
    },
    previewUrl() {
      return this.$axios.defaults.baseURL + this.url + '/preview'
    },
    canPreview() {
      return this.record.title && this.record.text && this.record.name_for && this.record.name_from
    },
    canSubmit() {
      return this.canPreview && this.record.email_from && /.+@.+\..+/.test(this.record.email_from)
    },
    loading() {
      return this.previewLoading || this.submitLoading
    },
    textLength() {
      return this.record.text ? this.record.text.length : 0
    },
  },
  watch: {
    'record.title'(newValue, oldValue) {
      if (!this.allowEmoji && newValue !== oldValue) {
        this.$nextTick(() => {
          this.record.title = this.filterText(newValue)
        })
      }
    },
    'record.text'(newValue, oldValue) {
      if (!this.allowEmoji && newValue !== oldValue) {
        this.$nextTick(() => {
          this.record.text = this.filterText(newValue)
          if (this.maxLength && this.textLength > this.maxLength) {
            this.record.text = this.record.text.slice(0, this.maxLength)
          }
        })
      }
    },
    'record.name_for'(newValue, oldValue) {
      if (!this.allowEmoji && newValue !== oldValue) {
        this.$nextTick(() => {
          this.record.name_for = this.filterText(newValue)
        })
      }
    },
    'record.name_from'(newValue, oldValue) {
      if (!this.allowEmoji && newValue !== oldValue) {
        this.$nextTick(() => {
          this.record.name_from = this.filterText(newValue)
        })
      }
    },
  },
  methods: {
    onFile(file) {
      if (file) {
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
          this.record.new_file = {file: reader.result, metadata: {name: file.name, size: file.size, type: file.type}}
        }
      }
    },
    async onPreview() {
      if (!this.canPreview) {
        return
      }
      this.previewLoading = true
      this.showPreview = false
      try {
        await this.$axios.post(this.url, {...this.record, preview: true})
        this.previewLoading = false
        this.showPreview = true
      } catch (e) {
        console.error(e)
      }
    },
    filterText(val) {
      return val.replace(
        /([\u2700-\u27BF]|[\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2011-\u26FF]|\uD83E[\uDD10-\uDDFF])/g,
        '',
      )
    },
    onTextInput(e) {
      const keys = ['Backspace', 'Delete', 'CapsLock', 'Tab', 'ArrowRight', 'ArrowLeft', 'ArrowUp', 'ArrowDown']
      if (!keys.includes(e.key) && !e.metaKey && !e.ctrlKey && !e.altKey && this.textLength >= this.maxLength) {
        e.preventDefault()
      }
    },
    async onSubmit() {
      if (!this.canSubmit) {
        return
      }
      this.submitLoading = true
      this.showPreview = false
      try {
        await this.$axios.post(this.url, {...this.record, preview: false})
        this.submitLoading = false
        this.showSuccess = true
        this.file = null
        this.record = {}
      } catch (e) {
        console.error(e)
      }
    },
  },
}
