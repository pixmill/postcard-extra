const path = require('path')
const AssetsManifest = require('webpack-assets-manifest')
const EslintPlugin = require('eslint-webpack-plugin')
const TerserJSPlugin = require('terser-webpack-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const {VueLoaderPlugin} = require('vue-loader')

const source = path.resolve('src/')
const dist = path.resolve('../assets/components/postcard/')

module.exports = (env, argv) => {
  return {
    entry: {
      mgr: path.resolve(source, 'mgr.js'),
      web: path.resolve(source, 'web.js'),
      'web-extended': path.resolve(source, 'web-extended.js'),
    },
    output: {
      path: path.resolve(dist),
      filename: 'js/[name].[hash:8].min.js',
    },
    target: 'web',
    optimization: {
      minimize: argv.mode === 'production',
      minimizer: [new TerserJSPlugin(), new CssMinimizerPlugin()],
    },
    module: {
      rules: [
        {
          test: /\.(s?)css$/,
          use: [
            {loader: MiniCssExtractPlugin.loader},
            {loader: 'css-loader'},
            {
              loader: 'sass-loader',
              options: {
                sassOptions: {
                  quietDeps: true,
                },
              },
            },
          ],
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
        },
        {
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
        },
      ],
    },
    plugins: [
      new EslintPlugin({
        context: source,
        extensions: ['js', 'vue'],
        cache: true,
        lintDirtyModulesOnly: true,
      }),
      new CleanWebpackPlugin({
        verbose: false,
        cleanStaleWebpackAssets: true,
        cleanOnceBeforeBuildPatterns:
          argv.mode === 'production' ? ['**/*', '!*.php'] : ['**/*', '!*.php', '!upload/**'],
      }),
      new MiniCssExtractPlugin({
        filename: 'css/[name].[hash:8].min.css',
      }),
      new AssetsManifest({
        output: path.resolve(dist, 'manifest.json'),
        publicPath: true,
      }),
      new VueLoaderPlugin(),
    ],
    devServer: {
      contentBase: path.resolve(dist),
      compress: true,
      port: 9000,
      watchContentBase: true,
      writeToDisk: true,
      disableHostCheck: true,
      hot: true,
      injectHot: true,
    },
    resolve: {
      extensions: ['.js', '.vue'],
      alias: {
        vue: 'vue/dist/vue.js',
      },
    },
  }
}
