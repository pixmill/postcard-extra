<?php
/** @var xPDOTransport $transport */

/** @var array $options */

use Phinx\Console\PhinxApplication;
use Phinx\Wrapper\TextWrapper;

if ($transport->xpdo) {
    switch ($options[xPDOTransport::PACKAGE_ACTION]) {
        case xPDOTransport::ACTION_INSTALL:
        case xPDOTransport::ACTION_UPGRADE:
            include_once MODX_CORE_PATH . 'components/postcard/bootstrap.php';
            $wrap = new TextWrapper(
                new PhinxApplication(),
                ['configuration' => MODX_CORE_PATH . 'components/postcard/phinx.php']
            );
            $result = $wrap->getMigrate();
            if ($wrap->getExitCode() !== 0) {
                $transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, print_r($result, true));
            }
            break;
        case xPDOTransport::ACTION_UNINSTALL:
            break;
    }
}

return true;