<?php

return [
    'tpl.PostCard.pdf' => [
        'file' => 'pdf.tpl',
        'description' => 'Template for rendered postcards',
    ],
    'tpl.PostCard.email' => [
        'file' => 'email.tpl',
        'description' => 'Template for email notification',
    ],
    'tpl.PostCard.form' => [
        'file' => 'form.vue',
        'description' => 'Vue form for frontend card submission',
    ],
    'tpl.PostCard.form-extended' => [
        'file' => 'form-extended.vue',
        'description' => 'Extended version of form with Vue code',
    ],
];