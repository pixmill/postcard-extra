<?php

return [
    'PostCard' => [
        'file' => 'postcard',
        'description' => 'Snippet for loading the postcard submission form',
        'properties' => [
            'tpl' => [
                'type' => 'textfield',
                'value' => 'tpl.PostCard.form',
            ],
            'loadJs' => [
                'type' => 'combo-boolean',
                'value' => true,
            ],
            'loadCss' => [
                'type' => 'combo-boolean',
                'value' => true,
            ],
            'extended' => [
                'type' => 'combo-boolean',
                'value' => false,
            ],
        ],
    ],
];