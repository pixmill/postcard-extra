<?php

return [
    'phone_mask' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'postcard_main',
    ],
    'max_text_length' => [
        'xtype' => 'textfield',
        'value' => '400',
        'area' => 'postcard_main',
    ],
    'allow_emoji' => [
        'xtype' => 'combo-boolean',
        'value' => false,
        'area' => 'postcard_main',
    ],
    'pdf_tpl' => [
        'xtype' => 'textfield',
        'value' => 'tpl.PostCard.pdf',
        'area' => 'postcard_main',
    ],
    'load_mgr_css' => [
        'xtype' => 'combo-boolean',
        'value' => true,
        'area' => 'postcard_main',
    ],
    'email_notify' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'postcard_main',
    ],
];