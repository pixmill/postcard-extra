<?php

if (!defined('MODX_CORE_PATH')) {
    $path = __DIR__;
    while (!file_exists($path . '/config.core.php') && (strlen($path) > 1)) {
        $path = dirname($path);
    }
    require $path . '/config.core.php';
}

return [
    'name' => 'PostCard',
    'name_lower' => 'postcard',
    'version' => '1.1.0',
    'release' => 'pl',
    'install' => true,
    'update' => [
        'chunks' => false,
        'settings' => false,
        'menus' => true,
        'snippets' => true,
    ],
    'static' => [
        'plugins' => false,
        'snippets' => false,
        'chunks' => false,
    ],
    'log_level' => 3,
    'log_target' => PHP_SAPI === 'cli' ? 'ECHO' : 'HTML',
];